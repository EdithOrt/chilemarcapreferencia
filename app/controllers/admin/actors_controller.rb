class Admin::ActorsController < Admin::AdminController
  before_action :set_actor, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource

  # GET /actors
  def index
    @actors = Actor.order(:name)
  end

  # GET /actors/1
  def show
  end

  # GET /actors/new
  def new
    @actor = Actor.new
  end

  # GET /actors/1/edit
  def edit
  end

  # POST /actors
  def create
    @actor = Actor.new(actor_params.except(:actor_positions_attributes))
    if @actor.save
      actor_params[:actor_positions_attributes].each do |actor_position|
        params_actor_position = actor_position[1]
        if !params_actor_position[:position_id].empty?
          params_actor_position['actor_id'] = @actor.id
          new_actor_position = ActorPosition.new( params_actor_position.except(:id) )
          if new_actor_position.save

          else
            p new_actor_position.errors
            render :new
          end
        end
      end
      redirect_to admin_actor_path(@actor), notice: 'Actor was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /actors/1
  def update
    actor_params[:actor_positions_attributes].each do |actor_position|
      question = Question.find(actor_position.first)
      p 'Question con id: ' + question.id.to_s + ' y pregunta: ' + question.title
      # chequear si existe una actor_position para este actor sobre este tema: actualizar si existe, crear si no existe
      actor_positions_for_actor = ActorPosition.where(actor: @actor)
      actor_positions_for_question = ActorPosition.where(position: question.positions)
      actor_position_for_question_and_actor = (actor_positions_for_actor & actor_positions_for_question)
      params_actor_position = actor_position[1]
      if actor_position_for_question_and_actor.any?
        p params_actor_position[:position_id]
        if params_actor_position[:position_id].empty?
          ActorPosition.delete(actor_position_for_question_and_actor)
        end
        actor_position_to_update = actor_position_for_question_and_actor.first
        actor_position_to_update.update(params_actor_position.except(:id))
      else
        params_actor_position['actor_id'] = @actor.id
        actor_position_about_new_topic = ActorPosition.new( params_actor_position.except(:id) )
        if actor_position_about_new_topic.save
          p ActorPosition.last
        else
          p actor_position_about_new_topic.errors
        end
        p question
      end
    end
    if @actor.update(actor_params.except(:actor_positions_attributes))
      redirect_to admin_actor_path(@actor), notice: 'Actor was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /actors/1
  def destroy
    @actor.destroy
    redirect_to admin_actors_url, notice: 'Actor was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_actor
      @actor = Actor.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def actor_params
      params.require(:actor).permit(:name, :description, :twitter, :logo, :actor_category_id, :content, :positions, actor_positions_attributes: [:position_id, :description, :id])
    end
end
