class Admin::AdminController < ActionController::Base
  layout "admin"
  load_and_authorize_resource

  def index
  end

  rescue_from CanCan::AccessDenied do
    if user_signed_in?
      redirect_back fallback_location: main_app.root_path, alert: I18n.t(:access_denied)
    else
      redirect_back fallback_location: main_app.root_path, alert: I18n.t(:access_denied)
    end
  end

  private

  def current_ability
    @current_ability ||= AdminAbility.new(current_user)
  end

end
