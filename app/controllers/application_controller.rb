class ApplicationController < ActionController::Base

  rescue_from CanCan::AccessDenied do
    if user_signed_in?
      redirect_back fallback_location: main_app.root_path, alert: I18n.t(:access_denied)
    else
      redirect_back fallback_location: main_app.root_path, alert: I18n.t(:access_denied)
    end
  end

end
