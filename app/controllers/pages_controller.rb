class PagesController < ApplicationController
  def index
    @actor_categories = ActorCategory.all
  end

  def about
  end

  def methodology
    @topics = Topic.all
  end

end
