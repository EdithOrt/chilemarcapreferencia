class TopicsController < ApplicationController
  before_action :set_topic, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource

  # GET /topics
  def index
    request.variant = determine_variant
    @topics = Topic.includes(questions: [positions: [actor_positions: [:actor, :position]]]).order(:name)

  end

  # GET /topics/1
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_topic
      @topic = Topic.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def topic_params
      params.require(:topic).permit(:title)
    end

    def determine_variant
      variant = :desktop
      # some code to determine the variant(s) to use
      variant = :mobile if browser.device.mobile?
      variant
    end

end
