class Position < ApplicationRecord
  belongs_to :question
  has_many :preferences
  has_many :actor_positions
  has_many :actors, through: :actor_positions
  has_one_attached :image_to_share
  has_one_attached :image_to_share_twitter

  # It isn't possible create positions with the same question and scale
  validates :scale, uniqueness: { scope: :question, message: "should be once per question" }

  def filter_by_actors(filtered_actors)
    actors.where(id: filtered_actors.pluck(:id))
  end

  def to_label
    "#{title}"
  end

  def actor_position_order_by_actor_category
    self.actor_positions.sort_by{|ap| ap.actor.actor_category.name}.reverse
  end

end
