class Question < ApplicationRecord

  belongs_to :topic
  has_many :positions
  accepts_nested_attributes_for :positions, allow_destroy: true

  validates :title, uniqueness: { case_sensitive: false }
  validates :title, presence: true

end
