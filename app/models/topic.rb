class Topic < ApplicationRecord
  resourcify

  has_many :questions
  has_one_attached :icon

  validates :name, uniqueness: { case_sensitive: false }
  validates :name, presence: true

end
