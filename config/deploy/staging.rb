server '172.105.151.82', user: 'deploy', roles: %w{app db web}

set :branch, 'staging'
set :deploy_to, '/home/deploy/staging'
set :stage, :staging
