Rails.application.routes.draw do

  namespace :admin do
    get '/' => 'admin#index'
    resources :actors
    resources :topics
    resources :questions
    resources :actor_categories
    resources :users
  end

  root 'pages#index'
  get "/about" => "pages#about"
  get "/methodology" => "pages#methodology"
  get "/index" => "pages#index"
  get "/index" => "pages#index"
  resources :topics, only: [:index]
  resources :preferences, only: [:create]
  get 'compara-preferencias', to: 'topics#index'
  get 'marca-tu-preferencia', action: :choose_topic, controller: :preferences
  get 'preferences/choose_topic', action: :choose_topic, controller: :preferences
  get 'preferences/answer_questions', action: :answer_questions, controller: :preferences
  get 'preferences/your_preferences', action: :your_preferences, controller: :preferences
  devise_for :users

end
