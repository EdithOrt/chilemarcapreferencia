class CreateActorPositions < ActiveRecord::Migration[6.0]
  def change
    create_table :actor_positions do |t|
      t.text :description

      t.timestamps
    end
  end
end
