class AddActorCategoryToActors < ActiveRecord::Migration[6.0]
  def change
    add_reference :actors, :actor_category
  end
end
