class AddPositionToPreferences < ActiveRecord::Migration[6.0]
  def change
    add_reference :preferences, :position
  end
end
