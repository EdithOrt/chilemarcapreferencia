require 'database_cleaner'
require 'csv'

DatabaseCleaner.allow_production = true

DatabaseCleaner.clean_with(:truncation)

user = User.create(email: "admin@example.org", password: 'admin123456')
user.add_role('admin')

csv_text_preferences = File.read(Rails.root.join('lib', 'seeds', 'preferences.csv'))
csv_preferences = CSV.parse(csv_text_preferences, :headers => true, :encoding => 'ISO-8859-1')
csv_preferences.each do |row|

  if Topic.exists?(name: row['topic'])
    topic = Topic.find_by(name: row['topic'])
  else
    topic = Topic.create(name: row['topic'])
    path = '/app/assets/images/' + row['icon']
    topic.icon.attach(io: File.open(File.join(Rails.root, path)), filename: topic.name)
  end

  if Question.exists?(title: row['question'])
    question = Question.find_by(title: row['question'])
  else
    question = Question.create(title: row['question'], topic: topic)
    puts ''
    puts '[TOPIC] ⚪ Se he creado topic: ' + topic.name
    puts '[QUESTION] ❔ ‍Con la pregunta: ' + question.title + ' y las siguientes preferencias:'
  end

  p = Position.create(title: row['preference'], question: question, scale: row['number'])
  puts '- [POSITION]: ' + p.title

  path_image_to_share_facebook = '/public/positions/' + row['facebook'] + '.png'
  puts path_image_to_share_facebook
  path_image_to_share_twitter = '/public/positions/' + row['twitter'] + '.png'
  puts path_image_to_share_twitter

  p.image_to_share.attach(io: File.open(File.join(Rails.root, path_image_to_share_facebook)), filename: p.title)
  p.image_to_share_twitter.attach(io: File.open(File.join(Rails.root, path_image_to_share_twitter)), filename: p.title)

end

puts ''
puts '✅✅✅ Topics, questions and positions created'
puts ''

# Actor categories
partido = ActorCategory.create(name: "Partidos políticos")
movimiento = ActorCategory.create(name: "Movimientos sociales")
organizacion = ActorCategory.create(name: "Organizaciones de la sociedad civil")

puts ''
puts '✅✅✅ Actor categories created'
puts ''

# Read csv with recommendations
csv_text = File.read(Rails.root.join('lib', 'seeds', '16_octubre_2020.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  
  actor_category = eval(row['actor_category'])
  a = Actor.create(actor_category: actor_category, name: row['name'])
  path_logo = '/app/assets/images/' + row['logo']
  a.logo.attach(io: File.open(File.join(Rails.root, path_logo)), filename: a.name)

  unless row['sist_gobierno'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Sistema de gobierno'))
    position = Position.find_by(title: row['sist_gobierno'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['sist_gobierno_desc'])
    end
  end
  unless row['economia'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Economía'))
    position = Position.find_by(title: row['economia'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['economia_desc'])
    end
  end
  unless row['prueblos_originarios'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Pueblos originarios'))
    position = Position.find_by(title: row['prueblos_originarios'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['prueblos_originarios_desc'])
    end
  end
  unless row['defensoria'].nil?
    question = Question.find_by(topic: Topic.find_by(name: "Defensoría del Pueblo"))
    position = Position.find_by(title: row['defensoria'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['defensoria_desc'])
    end
  end
  unless row['pensiones'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Sistema de pensiones'))
    position = Position.find_by(title: row['pensiones'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['pensiones_desc'])
    end
  end
  unless row['salud'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Sistema de salud'))
    position = Position.find_by(title: row['salud'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['salud_desc'])
    end
  end
  unless row['gratuidad_educacion'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Gratuidad de la educación superior'))
    position = Position.find_by(title: row['gratuidad_educacion'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['gratuidad_educacion_desc'])
    end
  end
  unless row['educacion'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Selección escolar'))
    position = Position.find_by(title: row['educacion'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['educacion_desc'])
    end
  end
  unless row['seguridad_ciudadana'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Seguridad ciudadana'))
    position = Position.find_by(title: row['seguridad_ciudadana'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['seguridad_ciudadana_desc'])
    end
  end
  unless row['genero'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Equidad de género'))
    position = Position.find_by(title: row['genero'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['genero_desc'])
    end
  end
  unless row['vivienda'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Vivienda'))
    position = Position.find_by(title: row['vivienda'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['vivienda_desc'])
    end
  end
  unless row['aborto'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Aborto'))
    position = Position.find_by(title: row['aborto'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['aborto_desc'])
    end
  end
  unless row['matrimonio_homosexual'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Matrimonio igualitario'))
    position = Position.find_by(title: row['matrimonio_homosexual'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['matrimonio_homosexual_desc'])
    end
  end
  unless row['infancia'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Infancia'))
    position = Position.find_by(title: row['infancia'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['infancia_desc'])
    end
  end
  unless row['derecho_a_huelga'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Derecho a huelga'))
    position = Position.find_by(title: row['derecho_a_huelga'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['derecho_a_huelga_desc'])
    end
  end
  unless row['negociacion_colectiva'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Negociación colectiva'))
    position = Position.find_by(title: row['negociacion_colectiva'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['negociacion_colectiva_desc'])
    end
  end
  unless row['migracion'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Política migratoria'))
    position = Position.find_by(title: row['migracion'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['migracion_desc'])
    end
  end
  unless row['agua'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Agua'))
    position = Position.find_by(title: row['agua'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['agua_desc'])
    end
  end
  unless row['cambio_climatico'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Cambio climático'))
    position = Position.find_by(title: row['cambio_climatico'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['cambio_climatico_desc'])
    end
  end
  unless row['tecnologia'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Tecnología'))
    position = Position.find_by(title: row['tecnologia'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['tecnologia_desc'])
    end
  end
  unless row['participacion_ciudadana'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Participación Ciudadana'))
    position = Position.find_by(title: row['participacion_ciudadana'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['participacion_ciudadana_desc'])
    end
  end
  unless row['trabajo_cuidado'].nil?
    question = Question.find_by(topic: Topic.find_by(name: 'Trabajo de Cuidados'))
    position = Position.find_by(title: row['trabajo_cuidado'], question: question)
    if position
      ActorPosition.create(actor: a, position: position, description: row['trabajo_cuidado_desc'])
    end
  end

  puts ' '
  puts '[ACTOR] ✊ Se he creado actor: ' + a.name + ' con las siguientes preferencias:'
  a.actor_positions.each do |ap|
    puts '- [' + ap.position.question.topic.name + ']' + ap.position.title
  end

end

puts ''
puts '✅✅✅ Actors and actor positions created'
puts ''
