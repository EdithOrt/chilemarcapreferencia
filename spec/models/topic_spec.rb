require 'rails_helper'

RSpec.describe Topic, type: :model do
  describe 'creation' do
    it 'can be created' do
      topic = Topic.create(name: 'Salud')
      expect(topic).to be_valid
    end

    it "can't be created two topics with the same name" do
      topic1 = Topic.create(name: "Educación")
      topic2 = Topic.create(name: "Educación")
      expect(topic2).to_not be_valid
    end
  end
end
