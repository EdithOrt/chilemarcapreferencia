require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'creation' do
    it 'can be created' do
      user = User.create(email: "test@test.org", password: '123456')
      expect(user).to be_valid
    end

    it 'email is neccesary to create user' do
      user = User.create(password: '123456')
      expect(user).to_not be_valid
    end

    it 'psw is neccesary to create user' do
      user = User.create(email: "test@test.org")
      expect(user).to_not be_valid
    end
  end
end
