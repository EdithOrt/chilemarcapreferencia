require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get index_url
    assert_response :success
    assert_select "title", "Home | Chile marca preferencia"
  end

  test "should get about page" do
    get about_url
    assert_response :success
    assert_select "title", "About | Chile marca preferencia"
  end

  test "should get root" do
   get root_url
   assert_response :success
 end

end
